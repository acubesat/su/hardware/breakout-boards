# AcubeSat - Breakout-Boards

Repository used for hosting the breakout PCB designs of the Science Unit.

**NOTE:** EVERYTHING CONTAINED HERE IS UNTESTED AND WORK-IN-PROGRESS.
USE AT YOUR OWN RISK!

# Requirements
You will need **KiCad version 6** to open the schematics.
Your KiCad installation should contain the standard footprint library
in order for the layout files to be displayed normally.

# Usage
Open the `.kicad_pro` file in KiCad and then:
* choose the `.kicad_sch` file to view the schematic
* choose the `.kicad_pcb` file to view the PCB's layout.
