<a href="https://www.flaticon.com/free-icons/heat" title="heat icons">Heat icon created by Smashicons - Flaticon</a>

### Installation

- On the top left toolbar, go to `Tools->External Plugins->Open Plugin Directory`
- Copy paste the `Heater_Generator_Plugin` directory inside the the plugin directory (unzipped)
- Press `Tools->External Plugins->Refresh Plugins`
- The Heater Generator Plugin should be visible on the top right with its red heater icon

<img src="plugin_icon_toolbar.png" alt="drawing" width="500"/>
