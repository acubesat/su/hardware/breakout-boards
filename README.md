# AcubeSat - Breakout-Boards

Repository used for hosting the breakout PCB designs of the Science Unit.

**NOTE:** EVERYTHING CONTAINED HERE IS UNTESTED AND WORK-IN-PROGRESS.
USE AT YOUR OWN RISK!

# Requirements
You will need **KiCad version 8** to open the schematics.
Your KiCad installation should contain the standard footprint library
in order for the layout files to be displayed normally.

# Usage
Open the `.kicad_pro` file in KiCad and then:
* choose the `.kicad_sch` file to view the schematic
* choose the `.kicad_pcb` file to view the PCB's layout.

# Git LFS Installation Guide

Git LFS (Large File Storage) is an extension for Git that helps manage large files like audio, video, and other binary files in your Git repository.

## Installation Instructions

### Windows

1. **Download and Install Git LFS**:
   - Go to the [Git LFS Releases](https://github.com/git-lfs/git-lfs/releases) page.
   - Download the `.exe` installer for Windows.
   - Run the installer and follow the installation instructions.

2. **Initialize Git LFS**:
   After installation, open a terminal (e.g., Git Bash) and run:
   ```bash
   git lfs install
   ```

3. **Verify Installation**:
   To confirm Git LFS is installed, check the version by running:
   ```bash
   git lfs --version
   ```

### Linux

1. **Install Git LFS**:
   Install Git LFS using your distribution's package manager:

   - **Debian/Ubuntu**:
     ```bash
     sudo apt install git-lfs
     ```

2. **Initialize Git LFS**:
   After installation, initialize Git LFS by running:
   ```bash
   git lfs install
   ```

3. **Verify Installation**:
   Verify the installation by checking the version:
   ```bash
   git lfs --version
   ```
